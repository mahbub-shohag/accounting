package accounting



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AccountsController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Accounts.list(params), model:[accountsInstanceCount: Accounts.count()]
    }

    def show(Accounts accountsInstance) {
        respond accountsInstance
    }

    def detail(Accounts accountInstance){
        respond accountInstance
    }

    def create() {
        respond new Accounts(params)
    }

    @Transactional
    def save(Accounts accountsInstance) {
        if (accountsInstance == null) {
            notFound()
            return
        }

        if (accountsInstance.hasErrors()) {
            respond accountsInstance.errors, view:'create'
            return
        }

        accountsInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'accounts.label', default: 'Accounts'), accountsInstance.id])
                redirect accountsInstance
            }
            '*' { respond accountsInstance, [status: CREATED] }
        }
    }

    def edit(Accounts accountsInstance) {
        respond accountsInstance
    }

    @Transactional
    def update(Accounts accountsInstance) {
        if (accountsInstance == null) {
            notFound()
            return
        }

        if (accountsInstance.hasErrors()) {
            respond accountsInstance.errors, view:'edit'
            return
        }

        accountsInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Accounts.label', default: 'Accounts'), accountsInstance.id])
                redirect accountsInstance
            }
            '*'{ respond accountsInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Accounts accountsInstance) {

        if (accountsInstance == null) {
            notFound()
            return
        }

        accountsInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Accounts.label', default: 'Accounts'), accountsInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'accounts.label', default: 'Accounts'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
