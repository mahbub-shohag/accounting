package accounting

class Accounts {
    int id
    String name
    String number
    Double openingBalance
    String bankName
    String bankPhone
    String bankAddress
    Boolean isActive
    Date createdAt
    Date updatedAt
    Date deletedAt
    static constraints = {
        name(blank: false, maxSize: 50)
        number(blank: false,maxSize: 20)
        openingBalance()
        bankName(inList: ['Bank Asia','Brack Bank','City Bank','DBBL'])
        bankAddress()
        bankPhone()
        isActive()
    }
}
