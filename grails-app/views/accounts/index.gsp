
<%@ page import="accounting.Accounts" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'accounts.label', default: 'Accounts')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-accounts" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-accounts" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'accounts.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="number" title="${message(code: 'accounts.number.label', default: 'Number')}" />
					
						<g:sortableColumn property="openingBalance" title="${message(code: 'accounts.openingBalance.label', default: 'Opening Balance')}" />
					
						<g:sortableColumn property="bankName" title="${message(code: 'accounts.bankName.label', default: 'Bank Name')}" />
					
						<g:sortableColumn property="bankAddress" title="${message(code: 'accounts.bankAddress.label', default: 'Bank Address')}" />
					
						<g:sortableColumn property="bankPhone" title="${message(code: 'accounts.bankPhone.label', default: 'Bank Phone')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${accountsInstanceList}" status="i" var="accountsInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${accountsInstance.id}">${fieldValue(bean: accountsInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: accountsInstance, field: "number")}</td>
					
						<td>${fieldValue(bean: accountsInstance, field: "openingBalance")}</td>
					
						<td>${fieldValue(bean: accountsInstance, field: "bankName")}</td>
					
						<td>${fieldValue(bean: accountsInstance, field: "bankAddress")}</td>
					
						<td>${fieldValue(bean: accountsInstance, field: "bankPhone")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>
	</body>
</html>
