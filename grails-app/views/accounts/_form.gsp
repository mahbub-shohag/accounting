<%@ page import="accounting.Accounts" %>



<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="accounts.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" maxlength="50" required="" value="${accountsInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'number', 'error')} required">
	<label for="number">
		<g:message code="accounts.number.label" default="Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="number" maxlength="20" required="" value="${accountsInstance?.number}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'openingBalance', 'error')} required">
	<label for="openingBalance">
		<g:message code="accounts.openingBalance.label" default="Opening Balance" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="openingBalance" value="${fieldValue(bean: accountsInstance, field: 'openingBalance')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'bankName', 'error')} required">
	<label for="bankName">
		<g:message code="accounts.bankName.label" default="Bank Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="bankName" from="${accountsInstance.constraints.bankName.inList}" required="" value="${accountsInstance?.bankName}" valueMessagePrefix="accounts.bankName"/>

</div>

<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'bankAddress', 'error')} required">
	<label for="bankAddress">
		<g:message code="accounts.bankAddress.label" default="Bank Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="bankAddress" required="" value="${accountsInstance?.bankAddress}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'bankPhone', 'error')} required">
	<label for="bankPhone">
		<g:message code="accounts.bankPhone.label" default="Bank Phone" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="bankPhone" required="" value="${accountsInstance?.bankPhone}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'isActive', 'error')} ">
	<label for="isActive">
		<g:message code="accounts.isActive.label" default="Is Active" />
		
	</label>
	<g:checkBox name="isActive" value="${accountsInstance?.isActive}" />

</div>

<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'createdAt', 'error')} required">
	<label for="createdAt">
		<g:message code="accounts.createdAt.label" default="Created At" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="createdAt" precision="day"  value="${accountsInstance?.createdAt}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'deletedAt', 'error')} required">
	<label for="deletedAt">
		<g:message code="accounts.deletedAt.label" default="Deleted At" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="deletedAt" precision="day"  value="${accountsInstance?.deletedAt}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: accountsInstance, field: 'updatedAt', 'error')} required">
	<label for="updatedAt">
		<g:message code="accounts.updatedAt.label" default="Updated At" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="updatedAt" precision="day"  value="${accountsInstance?.updatedAt}"  />

</div>

