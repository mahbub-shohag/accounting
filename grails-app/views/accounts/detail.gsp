<%--
  Created by IntelliJ IDEA.
  User: Abdur Rahman
  Date: 6/24/2019
  Time: 10:37 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <g:set var="entityName" value="${message(code: 'accounts.label', default: 'Accounts')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<g:if test="${accountsInstance?.isActive}">
    <li class="fieldcontain">
        <span id="isActive-label" class="property-label"><g:message code="accounts.isActive.label" default="Is Active" /></span>

        <span class="property-value" aria-labelledby="isActive-label"><g:formatBoolean boolean="${accountsInstance?.isActive}" /></span>

    </li>
</g:if>
<g:fieldValue bean="${accountsInstance}" field="name"/>
<g:fieldValue bean="${accontsInstance}" field="bankName"/>
</body>
</html>