
<%@ page import="accounting.Accounts" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'accounts.label', default: 'Accounts')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-accounts" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-accounts" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list accounts">
			
				<g:if test="${accountsInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="accounts.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${accountsInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountsInstance?.number}">
				<li class="fieldcontain">
					<span id="number-label" class="property-label"><g:message code="accounts.number.label" default="Number" /></span>
					
						<span class="property-value" aria-labelledby="number-label"><g:fieldValue bean="${accountsInstance}" field="number"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountsInstance?.openingBalance}">
				<li class="fieldcontain">
					<span id="openingBalance-label" class="property-label"><g:message code="accounts.openingBalance.label" default="Opening Balance" /></span>
					
						<span class="property-value" aria-labelledby="openingBalance-label"><g:fieldValue bean="${accountsInstance}" field="openingBalance"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountsInstance?.bankName}">
				<li class="fieldcontain">
					<span id="bankName-label" class="property-label"><g:message code="accounts.bankName.label" default="Bank Name" /></span>
					
						<span class="property-value" aria-labelledby="bankName-label"><g:fieldValue bean="${accountsInstance}" field="bankName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountsInstance?.bankAddress}">
				<li class="fieldcontain">
					<span id="bankAddress-label" class="property-label"><g:message code="accounts.bankAddress.label" default="Bank Address" /></span>
					
						<span class="property-value" aria-labelledby="bankAddress-label"><g:fieldValue bean="${accountsInstance}" field="bankAddress"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountsInstance?.bankPhone}">
				<li class="fieldcontain">
					<span id="bankPhone-label" class="property-label"><g:message code="accounts.bankPhone.label" default="Bank Phone" /></span>
					
						<span class="property-value" aria-labelledby="bankPhone-label"><g:fieldValue bean="${accountsInstance}" field="bankPhone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountsInstance?.isActive}">
				<li class="fieldcontain">
					<span id="isActive-label" class="property-label"><g:message code="accounts.isActive.label" default="Is Active" /></span>
					
						<span class="property-value" aria-labelledby="isActive-label"><g:formatBoolean boolean="${accountsInstance?.isActive}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountsInstance?.createdAt}">
				<li class="fieldcontain">
					<span id="createdAt-label" class="property-label"><g:message code="accounts.createdAt.label" default="Created At" /></span>
					
						<span class="property-value" aria-labelledby="createdAt-label"><g:formatDate date="${accountsInstance?.createdAt}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountsInstance?.deletedAt}">
				<li class="fieldcontain">
					<span id="deletedAt-label" class="property-label"><g:message code="accounts.deletedAt.label" default="Deleted At" /></span>
					
						<span class="property-value" aria-labelledby="deletedAt-label"><g:formatDate date="${accountsInstance?.deletedAt}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${accountsInstance?.updatedAt}">
				<li class="fieldcontain">
					<span id="updatedAt-label" class="property-label"><g:message code="accounts.updatedAt.label" default="Updated At" /></span>
					
						<span class="property-value" aria-labelledby="updatedAt-label"><g:formatDate date="${accountsInstance?.updatedAt}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:accountsInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${accountsInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
