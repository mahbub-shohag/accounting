<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<asset:stylesheet src="application.css"/>
		<asset:stylesheet src="style.css"/>
		<asset:javascript src="application.js"/>
		<asset:stylesheet src="bootstrap.css"/>
		<asset:javascript src="bootstrap.js"/>
		<g:layoutHead/>
	</head>
	<body>
		<div class="container-fluid">
			<div class="card card-default">
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							<div id="grailsLogo" role="banner"><a href="http://grails.org"><asset:image src="softrithm.png" alt="Softrithm"/></a></div>
						</div>
						<div class="col-md-2">
							<nav class="navbar bg-light sidebar">
								<ul class="navbar-nav">
									<li class="nav-item">
										<a class="nav-link" href="#">Dashboard</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#">Income</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="#">Expense</a>
									</li>
								</ul>
							</nav>
						</div>
						<div class="col-md-10">
							<g:layoutBody/>
						</div>
					</div>
				</div>
				<div class="card-footer pagefooter">
					<span>All Right Reserved by Softrithm IT Ltd</span>
				</div>
			</div>

		</div>

	</body>
</html>
